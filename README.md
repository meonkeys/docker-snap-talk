# docker-snap-talk

Hi there! This is a source repository for a talk on Docker and snap. Here are related links:

* https://osem.seagl.org/conferences/seagl2019/program/proposals/645
* https://adammonsen.com/post/1894/

And some related repositories:

* https://gitlab.com/meonkeys/docker-snap-talk
* https://gitlab.com/meonkeys/snap-imagemagick
* https://github.com/meonkeys/docker-imagemagick
* https://hub.docker.com/r/meonkeys/imagemagick

## Copyright and License

_Containerized sourcery 🧙 with Docker and Snap_, ©2019 Adam Monsen available for free under a Creative Commons Attribution-ShareAlike 4.0 International License.

For permissions beyond the scope of this license, contact Adam at <haircut@gmail.com>.
